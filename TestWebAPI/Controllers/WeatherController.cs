﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System;
using TestWebAPI.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TestWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly string apiKey = "91b7466cc755db1a94caf6d86a9c788a";
        private readonly string iconBaseUrl = "http://openweathermap.org/img/wn/";

        [HttpGet]
        public async Task<ActionResult> GetWeatherData()
        {
            try
            {
                List<Weather> cityWeatherData = new List<Weather>();

                List<int> cityIds = new List<int> { 1580578, 1581129, 1581297, 1581188, 1587923 };

                using (HttpClient client = new HttpClient())
                {
                    foreach (int cityId in cityIds)
                    {
                        string apiUrl = $"http://api.openweathermap.org/data/2.5/weather?id={cityId}&units=metric&appid={apiKey}";

                        HttpResponseMessage response = await client.GetAsync(apiUrl);

                        if (response.IsSuccessStatusCode)
                        {
                            string json = await response.Content.ReadAsStringAsync();
                            JObject weatherData = JsonConvert.DeserializeObject<JObject>(json);

                            Weather cityWeather = new Weather
                            {
                                CityId = cityId,
                                CityName = (string)weatherData["name"],
                                WeatherMain = (string)weatherData["weather"][0]["main"],
                                WeatherDescription = (string)weatherData["weather"][0]["description"],
                                WeatherIcon = $"{iconBaseUrl}{(string)weatherData["weather"][0]["icon"]}@2x.png",
                                MainTemp = (double)weatherData["main"]["temp"],
                                MainHumidity = (int)weatherData["main"]["humidity"]
                            };

                            cityWeatherData.Add(cityWeather);
                        }
                    }
                }

                var result = new
                {
                    data = cityWeatherData,
                    message = "Current weather information of cities",
                    statusCode = 200
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Error: {ex.Message}");
            }
        }
    }


}

