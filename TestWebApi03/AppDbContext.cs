﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TestWebApi03.Entity;

namespace TestWebApi03
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
        {
        }

        public AppDbContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<User> Users { get; set; }
    }
}
